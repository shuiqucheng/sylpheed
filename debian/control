Source: sylpheed
Section: mail
Priority: optional
Maintainer: Ricardo Mones <mones@debian.org>
Uploaders: Hideki Yamane <henrich@debian.org>,
 Kentaro Hayashi <hayashi@clear-code.com>
Build-Depends: debhelper-compat (= 12), dh-exec,
 libcompfaceg1-dev,
 libglib2.0-dev, libgtk2.0-dev, libpng-dev, libgpgme-dev (>= 1.0.0),
 libssl-dev, libldap2-dev, flex, bison, gettext,
 libreadline6-dev, libgtkspell-dev (>= 2.0.16-1.3~), libonig-dev,
 libenchant-2-dev,
Standards-Version: 4.5.0
Homepage: https://sylpheed.sraoss.jp/en/
Vcs-Git: https://salsa.debian.org/sylpheed-team/sylpheed.git
Vcs-Browser: https://salsa.debian.org/sylpheed-team/sylpheed
Rules-Requires-Root: no

Package: sylpheed
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: ${shlibs:Depends}, ${misc:Depends}, pinentry-gtk2,
 sensible-utils,
Recommends: sylpheed-i18n, sylfilter | bogofilter | bsfilter,
 aspell-en | aspell-dictionary, ca-certificates
Suggests: sylpheed-doc (>= 20020420-3), claws-mail-tools, curl,
 sylpheed-plugins,
Provides: mail-reader, news-reader
Description: Light weight e-mail client with GTK+
 Sylpheed is an e-mail client which aims for:
  * Quick response
  * Graceful, and sophisticated interface
  * Easy configuration, intuitive operation
  * Abundant features
 The appearance and interface are similar to some popular e-mail clients for
 Windows, such as Outlook Express or so.
 The interface is also designed to emulate the mailers on Emacsen, and almost
 all commands are accessible with the keyboard.

Package: sylpheed-i18n
Architecture: all
Section: localization
Depends: ${misc:Depends}, sylpheed (>= ${binary:Version})
Description: Locale data for Sylpheed (i18n support)
 This package provides support for non-English interfaces in the Sylpheed
 mail client.
 Current supported locales are:
  be (Belarusian)   bg (Bulgarian)      cs (Czech)
  da (Danish)       de (German)         el (Greek)
  es (Spanish)      et (Estonian)       eu (Basque)
  fi (Finnish)      fr (French)         gl (Galician)
  hr (Croatian)     hu (Hungarian)      it (Italian)
  ja (Japanese)     ko (Korean)         lt (Lithuanian)
  nl (Dutch)        pl (Polish)         pt_BR (Brazilian Portuguese)
  ro (Romanian)     ru (Russian)        sk (Slovak)
  sl (Slovenian)    sr (Serbian)        sv (Swedish)
  tr (Turkish)      uk (Ukrainian)      vi (Vietnamese)
  zh_CN (Chinese)   zh_TW (Taiwanese)

Package: sylpheed-plugins
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: ${shlibs:Depends}, ${misc:Depends}, sylpheed (= ${binary:Version})
Description: Loadable modules for extending Sylpheed features
 This package comprises all loadable plugins which are included
 with Sylpheed sources. Plugins extend Sylpheed by providing new
 features and/or capabilities not present in core application.
 .
 Currently only the unique existing plugin is included:
  Attachment Tool: a plugin for dealing with attachments in messages.
